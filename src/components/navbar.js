import React, { Component } from "react";
//import "./style.css";

export default class Navbar extends Component {
  render() {
    return (
      <div className="nav-container">
        <div className="titele-logo">
          <h3>
            <a href="#home">Shadwa Sameh</a>
          </h3>
        </div>
        <div className="nav-options">
          <ul>
            
            <li>
          {/* eslint-disable-next-line no-use-before-define  */}
              <a href="#top">About Me</a>
            </li>
            <li>
              <a href="#projects">My Skills</a>
            </li>
            <li>
              <a href="#about">Gallery</a>
            </li>
            <li>
              <a href="#contact">Contact Me</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
