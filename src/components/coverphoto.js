import React, { Component } from "react";
//import "./style.css";

import profileImage from "../components/image/profile.png";
export default class Coverphoto extends Component {
  render() {
    return (
      <div className="coverphoto-container" id="home">
        {/* <img
          className={this.props.photoClassName}
          src={this.props.coverimage}
          alt="companyphoto"
        /> */}
        <div className="wide">

        <h3>Hello I'm Shadwa</h3>
        <h1>Graphic Designer</h1>
            <h2>UX/UI Designer</h2>
            <a href="#About" class="home-btn">Know more</a>
            <div class="social-icons">
                <a href="https://www.behance.net/shadwasameh" target="blank"><i class="fa-brands fa-behance"></i></a>

            </div>
        </div>

        <div className="narrow">
            <img src={profileImage} alt="profile_picture"/>
        </div>

      </div>
    );
  }
}
