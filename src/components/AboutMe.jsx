import React, { Component } from 'react';

import profileImage from "./image/profile.png";
import './style.css';
class AboutMe extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div id="top">
                <h1 className="blueUnderline">About Me</h1>
                <div className="about-container" >
                    <div className="wide">

                        <h3 className="about-text">I am a graphic, ux/ui designer and animator.</h3>
                    </div>

                <div className="narrow">
                    <img src={profileImage} alt="profile_picture" />
                </div>

            </div>
            </div>
        );
    }
}

export default AboutMe;