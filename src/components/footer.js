import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-title">
          <h2>Contact Info</h2>
        </div>
        
        <div className="footer-contacts" id="contact">
          <ul>
            <li>
              <h4>Contacts</h4>
            </li>
            <li>Phone: 123456789</li>
            <li>Email: shadwasameh@gmail.com</li>
          </ul>
        </div>
      </div>
    );
  }
}

//{this.props.projectHeadTitle}
