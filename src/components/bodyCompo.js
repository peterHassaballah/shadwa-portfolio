import React, { Component } from "react";
import Projects from "./projects.js";
import GalleryCompo from "./galleryCompo.js";
import AboutMe from "./AboutMe";
//import "./style.css";

export default class BodyCompo extends Component {
  render() {
    return (
      <div className="body-container">
        <div className="body-content">
          <AboutMe />
          <Projects />
          <GalleryCompo />
        </div>
      </div>
    );
  }
}
