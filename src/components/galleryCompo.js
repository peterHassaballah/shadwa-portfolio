import React, { Component } from "react";
import AboutCard from "./aboutCard.js";
import Title from "./title.js";
// import PersonEmpty from "./image/personEmpty.png";
import Event1 from "./image/event.jpeg";
import Event2 from "./image/event2.jpeg";
import Event3 from "./image/event3.jpeg";
import Event4 from "./image/event.jpeg";

//import "./style.css";

export default class GalleryCompo extends Component {
  render() {
    return (
      <div className="Projects-container" id="about">
        <Title projectHeadTitle="Gallery" />
        
        <div className="projects-card">
          <AboutCard
            profileImage={Event1}
            nameTitle="Lorem Ipsum"
            positionTitle="Position"
            shortDesc=" Description"
          />
          <AboutCard
            profileImage={Event2}
            nameTitle="Lorem Ipsum"
            positionTitle="Position"
            shortDesc=" Description"
          />
          <AboutCard
            profileImage={Event3}
            nameTitle="Lorem Ipsum"
            positionTitle="Position"
            shortDesc=" Description"
          />
          <AboutCard
            profileImage={Event4}
            nameTitle="Lorem Ipsum"
            positionTitle="Position"
            shortDesc=" Description"
          />
          
        </div>
      </div>
    );
  }
}
