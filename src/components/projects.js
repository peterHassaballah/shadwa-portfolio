import React, { Component } from "react";
import ProjectCard from "./projectCard.js";
import ProjectOneImage from "./image/project-1.jpeg";
// import ProjectTwoImage from "./image/project-2.jpg";
// import ProjectThreeImage from "./image/project-3.jpg";
// import ProjectFourImage from "./image/project-4.jpg";
// import ProjectFiveImage from "./image/project-5.jpg";
// import ProjectSixImage from "./image/project-6.jpg";
// import ProjectSevenImage from "./image/project-7.jpg";
// import ProjectEightImage from "./image/project-8.jpg";
import "./style.css";

export default class Projects extends Component {
  render() {
    return (
      <div className="Projects-container" id="projects">
        <h1 className="blueUnderline">My Skills</h1>
       
        <div className="projects-card">
          <ProjectCard
            projectImage={ProjectOneImage}
            projectTitle="Lorem Ipsum"
          />
            <video width="750" height="500" controls >
              <source src="/video/videoplayback.mp4" type="video/mp4"/>
            </video>
        </div>
      </div>
    );
  }
}
